const trips = [
  {
    id: 1,
    title: 'Incredible India',
    date: 'September 18-20, 2016',
    category: 'india',
    locations: [
      {
        name: 'Taj Mahal, Agra, Uttar Pradesh',
        dec: [27.173891, 78.042068],
        gps: {
          lat: {
            deg: 27,
            min: 10
          },
          long: {
            deg: 78,
            min: 2
          }
        }
      },
      {
        name: 'Red Fort, Lal Qila, Delhi',
        dec: [28.656473, 77.242943],
        gps: {
          lat: {
            deg: 28,
            min: 39
          },
          long: {
            deg: 77,
            min: 14
          }
        }
      },
      {
        name: 'Golden Temple',
        dec: [31.633980, 74.872261],
        gps: {
          lat: {
            deg: 31,
            min: 38
          },
          long: {
            deg: 74,
            min: 52
          }
        }
      },
      {
        name: 'Charminar, Hyderabad',
        dec: [17.361431, 78.474533],
        gps: {
          lat: {
            deg: 17,
            min: 21
          },
          long: {
            deg: 78,
            min: 28
          }
        }
      }
    ],
    mapZoom: 5,
    excerpt: '',
    summaryPs: [
      'Taj Mahal is a monument located in Agra in India, constructed between 1631 and 1653 by a workforce of more than 20,000. The Mughal Emperor Shah Jahan commissioned its construction as a mausoleum for his favorite wife Mumtaz Mahal. ... While the white domed marble mausoleum is the most familiar part of the monument.'
    ],
    images: {
      featured: {
        src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/v1576155913/theri/famous%20place/tajmahal.jpg',
        w: 1800,
        h: 1260
      },
      others: [
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/v1576155904/theri/famous%20place/indian-red-fort.jpg',
          w: 1680,
          h: 1200
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/v1576155920/theri/famous%20place/golden-temple.jpg',
          w: 1680,
          h: 1200
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/v1576155922/theri/famous%20place/charminar-hydrabad.jpg',
          w: 1680,
          h: 1200
        }
      ]
    }
  },
  {
    id: 2,
    title: 'Rajasthan',
    date: 'July 18-29, 2018',
    category: 'rajasthan',
    locations: [
      {
        name: 'Jaipur',
        dec: [26.9220704,75.778885],
        gps: {
          lat: {
            deg: 26,
            min: 55
          },
          long: {
            deg: 75,
            min: 46
          }
        }
      },
      {
        name: 'Jodhpur',
        dec: [26.263863,73.008957],
        gps: {
          lat: {
            deg: 26,
            min: 15
          },
          long: {
            deg: 73,
            min: 0
          }
        }
      },
      {
        name: 'Udaipur',
        dec: [24.571270,73.691544],
        gps: {
          lat: {
            deg: 24,
            min: 34
          },
          long: {
            deg: 73,
            min: 41
          }
        }
      },
      {
        name: 'Jaisalmer',
        dec: [26.911661,70.922928],
        gps: {
          lat: {
            deg: 26,
            min: 54
          },
          long: {
            deg: 70,
            min: 55
          }
        }
      }
    ],
    mapZoom: 6,
    excerpt: 'Padharo mahare desh',
    summaryPs: [
      'Rajasthan is beautiful and largest state of India. Jaipur  (Pink City) is capital of Rajasthan. People speak here Rajasthani Language.  The main geographic features of Rajasthan are the Thar Desert and the Aravalli Range, Mount Abu, Raisina Hill, Thar, Luni River and may more.',
      'The palaces of Jaipur, lakes of Udaipur, and desert forts of Jodhpur, Bikaner & Jaisalmer rank among the most preferred destinations in India for many tourists both Indian and foreign.'
    ],
    images: {
      featured: {
        src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/c_scale,h_600,w_1000/v1576176137/theri/rajasthan/udaipur-hotel_igps9h.jpg',
        w: 1000,
        h: 600
      },
      others: [
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/v1576176144/theri/rajasthan/hava_mahal_xwg72q.jpg',
          w: 1680,
          h: 1260
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/c_scale,h_1680,w_1260/upload/v1576176139/theri/rajasthan/lake_palace_x0zh8t.jpg',
          w: 1680,
          h: 1260
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/c_scale,h_1680,w_1260/upload/v1576176127/theri/rajasthan/Jodhpur_City_ye53dj.jpg',
          w: 1680,
          h: 1260
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/c_scale,h_1680,w_1260/upload/v1576176125/theri/rajasthan/jaisalamar_taeswh.jpg',
          w: 1680,
          h: 1260
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/c_scale,h_1680,w_1260/upload/v1576176140/theri/rajasthan/fort_f6tei5.jpg',
          w: 1680,
          h: 1260
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/c_scale,h_1680,w_1260/upload/v1576176124/theri/rajasthan/rajasthan-desert_kwxqu5.jpg',
          w: 1680,
          h: 1260
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/c_scale,h_1680,w_1260/upload/v1576176125/theri/rajasthan/Jantar_Mantar_Jaipur_a9kzou.jpg',
          w: 1680,
          h: 1260
        },
      ]
    }
  },
  {
    id: 3,
    title: 'Their survival is in your hands',
    date: 'May 30, 2017',
    category: 'himalaya',
    locations: [
      {
        name: 'Himalaya Mountain',
        dec: [28.5983,83.9311],
        gps: {
          lat: {
            deg: 28,
            min: 35
          },
          long: {
            deg: 83,
            min: 55
          }
        }
      }
    ],
    mapZoom: 8,
    excerpt: 'Every so often it becomes apparent that the world can be pretty inspiring, even in your own backyard.',
    summaryPs: [
      "Himalaya is a mountain range in Asia, separating the Indian subcontinent from the Tibetan Plateau. ... The Himalaya stretches across five nations, Bhutan, China, India, Nepal, and Pakistan. It is the source of three of the world's major river systems, the Indus Basin, the Ganga-Brahmaputra Basin and the Yangtze Basin."
    ],
    images: {
      featured: {
        src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/c_scale,w_1680/v1576155870/theri/himalaya/himalaya-1.jpg',
        w: 1680,
        h: 1120
      },
      others: [
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/c_scale,h_1120,w_1680/v1576155826/theri/himalaya/himalaya-2.jpg',
          w: 1680,
          h: 1120
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/c_scale,h_1120,w_1680/v1576155812/theri/himalaya/himalaya-3.jpg',
          w: 1680,
          h: 1120
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/c_scale,h_1120,w_1680/v1576155801/theri/himalaya/himalaya-4.jpg',
          w: 1680,
          h: 1120
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/c_scale,h_1200,w_800/v1576155822/theri/himalaya/himalaya-5.jpg',
          w: 799,
          h: 1200
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/c_scale,h_1260,w_900/v1576155815/theri/himalaya/himalaya-6.jpg',
          w: 900,
          h: 1260
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/c_scale,h_1200,w_1680/v1576155831/theri/himalaya/himalaya-7.jpg',
          w: 1680,
          h: 1116
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/c_scale,h_1200,w_1680/v1576155840/theri/himalaya/himalaya-9.jpg',
          w: 1680,
          h: 1200
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/c_scale,h_1200,w_1680/v1576155823/theri/himalaya/himalaya-10.jpg',
          w: 1680,
          h: 1200
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/c_scale,h_1400,w_1000/v1576155832/theri/himalaya/himalaya-8.jpg',
          w: 1000,
          h: 1400
        },
      ]
    }
  },
  {
    id: 4,
    title: 'A Perfect Holiday Destination',
    date: 'March 18-19, 2019',
    category: 'goa',
    locations: [
      {
        name: 'Panjim, Goa',
        dec: [15.496777,73.827827],
        gps: {
          lat: {
            deg: 15,
            min: 29
          },
          long: {
            deg: 73,
            min: 49
          }
        }
      }
    ],
    mapZoom: 5,
    excerpt: 'Best part of the day is the sunrise and the rainbow while the stars and the moon in the naked sky are the favorite part of our night.',
    summaryPs: [
      'Goa is a coastal region located in the western part of India famous as most popular tourist destination.',
      'Goa is one of the most important tourist spots in the country because of its beauty and culture. If you love beach and adventure activities, this is the best place to enjoy.'
    ],
    images: {
      featured: {
        src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/c_scale,h_1200,w_1680/v1576155922/theri/goa/Querim_Beach_Goa.jpg',
        w: 1680,
        h: 1200
      },
      others: [
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/c_scale,h_1200,w_1680/v1576155910/theri/goa/goa-church.jpg',
          w: 1680,
          h: 1200
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/c_scale,h_1200,w_1680/v1576155920/theri/goa/goa-gJhev0YgUcE-unsplash.jpg',
          w: 1680,
          h: 1200
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/c_scale,h_1200,w_1680/v1576155921/theri/goa/goa-sunset.jpg',
          w: 1680,
          h: 1200
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/c_scale,h_1180,w_1500/v1576155922/theri/goa/Querim_Beach_Goa.jpg',
          w: 1180,
          h: 1500
        }
      ]
    }
  },
  {
    id: 5,
    title: 'Delhi',
    date: 'August 5, 2016',
    category: 'metro',
    locations: [
      {
        name: 'Dildaar Dilli!',
        dec: [28.644800,77.216721],
        gps: {
          lat: {
            deg: 28,
            min: 38
          },
          long: {
            deg: 77,
            min: 13
          }
        }
      },
    ],
    mapZoom: 7,
    excerpt: 'I asked my soul: What is Delhi? She replied: the word is my body and Delhi its life.',
    summaryPs: [
      'Delhi is a metropolis in northern India. Delhi derives its historic importance from its position in northern India between the Aravalli Hills to the southwest and the Yamuna river on whose western banks it stands. This enabled it to dominate the old trade routes from northwest India to the plains of the Ganges.',
    ],
    images: {
      featured: {
        src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/c_scale,h_1260,w_1680/v1576155878/theri/metro/delhi/delhi-3_ftakof_twzdpw.jpg',
        w: 1680,
        h: 1200
      },
      others: [
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/c_scale,h_820,w_980/v1576155849/theri/metro/delhi/delhi-1_sou0hl_vcgrgd.jpg',
          w: 1680,
          h: 1200
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/c_scale,h_1120,w_1280/v1576155847/theri/metro/delhi/delhi-4_e4f0i3_hvuaym.jpg',
          w: 1680,
          h: 1200
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/c_scale,h_1260,w_1280/v1576155840/theri/metro/delhi/delhi-2_uellez_ifkmeh.jpg',
          w: 1680,
          h: 600
        }
      ]
    }
  },
  {
    id: 6,
    title: 'Mumbai',
    date: 'August 16, 2016',
    category: 'metro',
    locations: [
      {
        name: 'Aamchi Mumbai',
        dec: [19.076090,72.877426],
        gps: {
          lat: {
            deg: 19,
            min: 4
          },
          long: {
            deg: 72,
            min: 52
          }
        }
      },
    ],
    mapZoom: 6,
    excerpt: 'Used to summer camp here as a kid, revisiting it as an adult doesn\'t dimish the scale.',
    summaryPs: [
      "Mumbai is the commercial capital of India. It is also known as the city that never sleeps. Mumbai is the perfect blend of culture, customs and lifestyles. Mumbai is India's most cosmopolitan city, its financial powerhouse and the nerve center of India's fashion industry."
    ],
    images: {
      featured: {
        src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/c_scale,h_1120,w_1680/v1576155892/theri/metro/mumbai/mumbai-6_hcxzoj_oubjrl.jpg',
        w: 1680,
        h: 1120
      },
      others: [
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/c_scale,h_1120,w_1680/v1576155861/theri/metro/mumbai/mumbai-1_vupf9n_z9t7xn.jpg',
          w: 1680,
          h: 1120
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/c_scale,h_1120,w_1680/v1576155874/theri/metro/mumbai/mumbai-2_rcizrj_lnzye4.jpg',
          w: 1680,
          h: 1120
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/c_scale,h_1120,w_1680/v1576155885/theri/metro/mumbai/mumbai-3_zj9cqy_zx6tmd.jpg',
          w: 1680,
          h: 1120
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/c_scale,h_1120,w_1680/v1576155876/theri/metro/mumbai/mumbai-5_c7vwfz_oyd7kd.jpg',
          w: 1680,
          h: 1120
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/c_scale,h_1120,w_1680/v1576155872/theri/metro/mumbai/mumbai-4_n9sleu_ar8klp.jpg',
          w: 1680,
          h: 1120
        }
      ]
    }
  },
  {
    id: 7,
    title: 'Kolkata',
    date: 'Jan 6-8, 2016',
    category: 'metro',
    locations: [
      {
        name: 'City of Joy',
        dec: [22.572645,88.363892],
        gps: {
          lat: {
            deg: 22,
            min: 34
          },
          long: {
            deg: 88,
            min: 21
          }
        }
      }
    ],
    mapZoom: 6,
    excerpt: 'Welcome to Bengal, the sweetest part of India',
    summaryPs: [
      'Kolkata is the capital of the Indian state of West Bengal and was capital of British India until 1912.The urban agglomeration of Kolkata covers several municipal corporations, municipalities, city boards and villages and is the third largest urban agglomeration in India after Mumbai and Delhi.'
    ],
    images: {
      featured: {
        src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/c_scale,h_800,w_1680/v1576155849/theri/metro/kolkata/kolkata-4_b0bemq_em1xek.jpg',
        w: 1680,
        h: 800
      },
      others: [
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/c_scale,h_1120,w_1680/v1576155853/theri/metro/kolkata/kolkata-1_ugtju4_aje4z3.jpg',
          w: 1680,
          h: 1120
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/c_scale,h_894,w_1680/v1576155863/theri/metro/kolkata/kolkata-2_lxmlwx_qdxd3j.jpg',
          w: 1680,
          h: 894
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/c_scale,h_894,w_1680/v1576155848/theri/metro/kolkata/kolkata-3_fpglh6_adtjty.jpg',
          w: 1680,
          h: 894
        }
      ]
    }
  },
  {
    id: 8,
    title: 'Bangalore',
    date: 'Feb 2-5, 2016',
    category: 'metro',
    locations: [
      {
        name: 'Namma Bengaluru',
        dec: [12.971599,77.594566],
        gps: {
          lat: {
            deg: 12,
            min: 58
          },
          long: {
            deg: 77,
            min: 35
          }
        }
      }
    ],
    mapZoom: 6,
    excerpt: 'IT HUB/SILICON VALLEY/GARDEN CITY of India',
    summaryPs: [
      "The word Bangalore came from the Kannada word Bengaluru which means town of boiled beans. Bangalore, the Capital of the south Indian state of Karnataka is better known as Silicon Valley of India, a hub of most of the tech companies around the world.",
      "Bangalore is also known as the Pub City with the highest number of pubs in any Indian city and as Garden City for its year round blossoms and greenery."
    ],
    images: {
      featured: {
        src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/c_scale,h_700,w_1680/v1576155824/theri/metro/bengaluru/bengaluru-1.jpg',
        w: 1680,
        h: 700
      },
      others: [
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/c_scale,h_1120,w_1680/v1576255797/theri/metro/bengaluru/bengaluru-2.jpg',
          w: 1680,
          h: 1120
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/c_scale,h_894,w_1680/v1576255801/theri/metro/bengaluru/bengaluru-3.png',
          w: 1680,
          h: 894
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/c_scale,h_894,w_1680/v1576255797/theri/metro/bengaluru/bengaluru-4.jpg',
          w: 1680,
          h: 894
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/c_scale,h_894,w_1680/v1576255804/theri/metro/bengaluru/bengaluru-5.jpg',
          w: 1680,
          h: 894
        }
      ]
    }
  } 
];

export default trips;
